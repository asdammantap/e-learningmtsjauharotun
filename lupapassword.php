<html>
<head>
<title>E-Learning MA. Al-Jauharatunnaqiyah</title>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'/>
 <script src="js/bootstrap.js"></script>
 <link rel="stylesheet" type="text/css" href="assets/csslogin/bootstrap-theme.css">
 <link rel="stylesheet" type="text/css" href="assets/csslogin/bootstrap.css">
<script src="assets/js/login.js"></script>
<link rel="stylesheet" href="assets/csslogin/style.css">
<!--[if lt IE 9]>
 <script src=”https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js”></script>
   <script src=”https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js”></script>
<![endif]-->    
<link rel="icon" type="image/png" id="favicon"
          href="img/logojauharotunnaqiyah.png"/>
</head>
<body>
    <div class="container">
	<div class="banner"><center>
	</div>
        <div class="card card-container">
            <img id="profile-img" class="profile-img-card" src="img/logojauharotunnaqiyah.jpg" />
            <p id="profile-name" class="profile-name-card"></p>
            <form method="post" action="proseslupapassword.php">
                <input type="text" class="form-control" placeholder="Username" name="username" required autofocus>
                </br><input type="password" class="form-control" placeholder="Password" name="password" required>
				 </br><input type="password" class="form-control" placeholder="Confirm Password" name="password" required>
                <div id="remember" class="checkbox">
                    </div>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Ganti Password</button>
			</form>
           <a href="index.php" class="forgot-password">
               Go To Login Page
            </a>
        </div><!-- /card-container -->
    </div><!-- /container -->
</body>
</html>
