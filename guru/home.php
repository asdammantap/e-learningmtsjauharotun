<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>
<body>
<div class="row-fluid">
                        <div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
                            <h4>Welcome</h4>
                        	Selamat Datang <?php echo "$_SESSION[username]";?> Di Halaman Utama E-Learning MA. AL-JAUHAROTUNNAQIYAH</div>
                        	<div class="navbar">
                            	<div class="navbar-inner">
	                                <ul class="breadcrumb">
	                                    <i class="icon-chevron-left hide-sidebar"><a href='#' title="Hide Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <i class="icon-chevron-right show-sidebar" style="display:none;"><a href='#' title="Show Sidebar" rel='tooltip'>&nbsp;</a></i>
	                                    <li>
	                                        <a href="#">Dashboard</a> <span class="divider">/</span>	
	                                    </li>
	                                 </ul>
                            	</div>
                        	</div>
                    	</div>
					<div class="row-fluid">
                        <!-- block -->
						 <div class="block span12" style="height:300px;">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Fitur E-Learning MA.AL-JAUHAROTUNNAQIYAH</div>
                               </div>
							
                        <div class="block span3" style="height:220px;">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Profil Pribadi</div>
                                <div class="pull-right"><span></span>
								</div>
                            </div>
							<center><a href="?p=profil"><img src="images/profilicon.png">
							<div class="chart-bottom-heading"><span class="label label-info">Profil Pribadi</span>
						    </div></a>
                           </div>
                        <!-- /block -->
                    
					<div class="block span3" style="height:220px;margin-left:3px;">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Materi Pelajaran</div>
                                <div class="pull-right"><span></span>
								</div>
								</div>
							<center><a href="?p=datamateri"><img src="images/materiicon.png" style="margin-top:10px;">
							<div class="chart-bottom-heading"><span class="label label-info">Materi Pelajaran</span>
						    </div></a>
                           </div>
                   <div class="block span3" style="height:220px;margin-left:3px;">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Tugas Dari Guru Mapel</div>
                                <div class="pull-right"><span></span>
								</div>
                            </div>
							<center><a href="?p=datatugas"><img src="images/tugasicon.png" style="margin-top:10px;">
							<div class="chart-bottom-heading"><span class="label label-info">Tugas Mata Pelajaran</span>
						    </div></a>
                      </div>
					  <div class="block span3" style="height:220px;margin-left:3px;">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left">Ujian Mata Pelajaran</div>
                                <div class="pull-right"><span></span>
								</div>
                            </div>
							<center><a href="?p=dataujian"><img src="images/examicon.png" style="margin-top:10px;">
							<div class="chart-bottom-heading"><span class="label label-info">Ujian Mata Pelajaran</span>
						    </div></a>
                      </div>
					  </div>
					  </div>
                        <!--/.fluid-container-->
        <script>
        $(function() {
            // Easy pie charts
            $('.chart').easyPieChart({animate: 1000});
        });
        </script>
</body>
</html>