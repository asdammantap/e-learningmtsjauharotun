-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 27, 2016 at 02:06 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `weblearningjauharotun2`
--

-- --------------------------------------------------------

--
-- Table structure for table `banksoalpg`
--

CREATE TABLE IF NOT EXISTS `banksoalpg` (
  `id_soal` char(5) NOT NULL DEFAULT '',
  `kd_mp` char(6) NOT NULL DEFAULT '',
  `soal` text,
  `pil_a` varchar(200) DEFAULT NULL,
  `pil_b` varchar(200) DEFAULT NULL,
  `pil_c` varchar(200) DEFAULT NULL,
  `pil_d` varchar(200) DEFAULT NULL,
  `kunci_jwb` enum('A','B','C','D') DEFAULT NULL,
  PRIMARY KEY (`id_soal`),
  KEY `kd_mk` (`kd_mp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banksoalpg`
--

INSERT INTO `banksoalpg` (`id_soal`, `kd_mp`, `soal`, `pil_a`, `pil_b`, `pil_c`, `pil_d`, `kunci_jwb`) VALUES
('PG001', '111', 'What Is An Object??', 'Object Is Some one', 'Object Is Something', 'Object Is Everything', 'I Dont Know', 'D'),
('PG002', '111', 'What Is Love?', 'Love Is You', 'Love Is Feel', 'Love Is Money', 'Love Is Who Are You', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `guru`
--

CREATE TABLE IF NOT EXISTS `guru` (
  `nip` char(11) NOT NULL DEFAULT '',
  `namaguru` varchar(50) DEFAULT NULL,
  `jeniskelamin` varchar(10) DEFAULT NULL,
  `alamat` text,
  `tempatlahir` varchar(30) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `guru`
--

INSERT INTO `guru` (`nip`, `namaguru`, `jeniskelamin`, `alamat`, `tempatlahir`, `tgllahir`, `email`) VALUES
('-', NULL, NULL, NULL, NULL, NULL, NULL),
('001', 'doni', 'laki-laki', 'waringin kurung', 'serang', '1999-02-17', 'sarif@yahoo.com'),
('002', 'mila', 'perempuan', 'kubang gabus', 'cilegon', '1986-09-20', 'mila@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `gurump`
--

CREATE TABLE IF NOT EXISTS `gurump` (
  `nip` char(11) NOT NULL DEFAULT '',
  `kd_mp` char(6) NOT NULL DEFAULT '',
  `kd_kelas` char(5) NOT NULL DEFAULT '',
  `thn_ajaran` char(4) NOT NULL DEFAULT '',
  PRIMARY KEY (`nip`,`kd_mp`,`kd_kelas`),
  KEY `kd_mk` (`kd_mp`),
  KEY `kd_kelas` (`kd_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gurump`
--

INSERT INTO `gurump` (`nip`, `kd_mp`, `kd_kelas`, `thn_ajaran`) VALUES
('001', '111', 'XIIPA', '2015');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE IF NOT EXISTS `kelas` (
  `kd_kelas` char(5) NOT NULL DEFAULT '',
  `thn_ajaran` char(4) DEFAULT NULL,
  `nip` char(11) DEFAULT NULL,
  PRIMARY KEY (`kd_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`kd_kelas`, `thn_ajaran`, `nip`) VALUES
('-', NULL, NULL),
('IE', '2009', '001'),
('XIIPA', '2013', '001');

-- --------------------------------------------------------

--
-- Table structure for table `matapelajaran`
--

CREATE TABLE IF NOT EXISTS `matapelajaran` (
  `kd_mp` char(6) NOT NULL DEFAULT '',
  `nma_mp` varchar(30) DEFAULT NULL,
  `kurikulum` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`kd_mp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matapelajaran`
--

INSERT INTO `matapelajaran` (`kd_mp`, `nma_mp`, `kurikulum`) VALUES
('-', NULL, NULL),
('111', 'Bahasa Inggris', '2015');

-- --------------------------------------------------------

--
-- Table structure for table `materi`
--

CREATE TABLE IF NOT EXISTS `materi` (
  `kd_materi` int(11) NOT NULL AUTO_INCREMENT,
  `kd_mp` char(6) NOT NULL DEFAULT '',
  `kd_kelas` char(5) NOT NULL DEFAULT '',
  `judul_materi` varchar(100) DEFAULT NULL,
  `filename` text,
  PRIMARY KEY (`kd_materi`),
  KEY `kd_mk` (`kd_mp`),
  KEY `kd_kelas` (`kd_kelas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `materi`
--

INSERT INTO `materi` (`kd_materi`, `kd_mp`, `kd_kelas`, `judul_materi`, `filename`) VALUES
(2, '111', 'XIIPA', 'Noun', 'lamaran.docx');

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE IF NOT EXISTS `nilai` (
  `id_jawaban` char(5) NOT NULL DEFAULT '',
  `id_soalujian` char(5) DEFAULT '',
  `kd_mk` char(6) DEFAULT '',
  `kd_kelas` char(5) DEFAULT '',
  `nim` char(11) DEFAULT '',
  `nilaiakhir` float(5,0) DEFAULT NULL,
  PRIMARY KEY (`id_jawaban`),
  KEY `id_soalujian` (`id_soalujian`),
  KEY `kd_mk` (`kd_mk`),
  KEY `kd_kelas` (`kd_kelas`),
  KEY `nim` (`nim`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE IF NOT EXISTS `pengguna` (
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(20) NOT NULL,
  `level` varchar(15) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`username`, `password`, `level`) VALUES
('001', '123', 'Guru'),
('01-22-2016', '123', 'Siswa'),
('admin', 'admin', 'Administrator'),
('doni', 'doni', 'Administrator');

-- --------------------------------------------------------

--
-- Stand-in structure for view `query_ikutujian`
--
CREATE TABLE IF NOT EXISTS `query_ikutujian` (
`id_soalujian` char(5)
,`kd_mp` char(6)
,`kd_kelas` char(5)
,`nip` char(11)
,`waktu` varchar(10)
,`tipe` enum('UHAR','UTS','UAS')
,`tgl_ujian` date
,`nma_mp` varchar(30)
,`namaguru` varchar(50)
,`namasiswa` varchar(50)
,`nis` char(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `query_ikutujiantipe`
--
CREATE TABLE IF NOT EXISTS `query_ikutujiantipe` (
`id_soalujian` char(5)
,`kd_mp` char(6)
,`kd_kelas` char(5)
,`nip` char(11)
,`waktu` varchar(10)
,`tipe` varchar(21)
,`tgl_ujian` date
,`nma_mp` varchar(30)
,`namaguru` varchar(50)
,`namasiswa` varchar(50)
,`nis` char(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `query_tampilbanksoal`
--
CREATE TABLE IF NOT EXISTS `query_tampilbanksoal` (
`id_soal` char(5)
,`nma_mp` varchar(30)
,`soal` text
,`pil_a` varchar(200)
,`pil_b` varchar(200)
,`pil_c` varchar(200)
,`pil_d` varchar(200)
,`kunci_jwb` enum('A','B','C','D')
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `query_tampilbanksoalgurump`
--
CREATE TABLE IF NOT EXISTS `query_tampilbanksoalgurump` (
`id_soal` char(5)
,`nma_mp` varchar(30)
,`soal` text
,`pil_a` varchar(200)
,`pil_b` varchar(200)
,`pil_c` varchar(200)
,`pil_d` varchar(200)
,`kunci_jwb` enum('A','B','C','D')
,`nip` char(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `query_tampilgurump`
--
CREATE TABLE IF NOT EXISTS `query_tampilgurump` (
`nip` char(11)
,`namaguru` varchar(55)
,`nma_mp` varchar(30)
,`kd_kelas` char(5)
,`thn_ajaran` char(4)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `query_tampilgurumpfull`
--
CREATE TABLE IF NOT EXISTS `query_tampilgurumpfull` (
`nip` char(11)
,`namaguru` varchar(50)
,`kd_mp` char(6)
,`nma_mp` varchar(30)
,`kd_kelas` char(5)
,`thn_ajaran` char(4)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `query_tampilkelas`
--
CREATE TABLE IF NOT EXISTS `query_tampilkelas` (
`kd_kelas` char(5)
,`thn_ajaran` char(4)
,`namaguru` varchar(55)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `query_tampilmateri`
--
CREATE TABLE IF NOT EXISTS `query_tampilmateri` (
`kd_materi` int(11)
,`nma_mp` varchar(30)
,`kd_kelas` char(5)
,`judul_materi` varchar(100)
,`filename` text
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `query_tampiltugas`
--
CREATE TABLE IF NOT EXISTS `query_tampiltugas` (
`kd_tugas` int(11)
,`nma_mp` varchar(30)
,`kd_kelas` char(5)
,`judul_tugas` varchar(100)
,`filename` text
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `query_ujian`
--
CREATE TABLE IF NOT EXISTS `query_ujian` (
`id_soalujian` char(5)
,`id_soal` char(5)
,`soal` text
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `query_walikelas`
--
CREATE TABLE IF NOT EXISTS `query_walikelas` (
`nip` char(11)
,`namaguru` varchar(55)
,`alamat` text
,`email` varchar(30)
,`kd_kelas` char(5)
);
-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE IF NOT EXISTS `siswa` (
  `nis` char(11) NOT NULL DEFAULT '',
  `namasiswa` varchar(50) DEFAULT NULL,
  `jeniskelamin` varchar(10) DEFAULT NULL,
  `alamat` text,
  `tempatlahir` varchar(30) DEFAULT NULL,
  `tgllahir` date DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `kd_kelas` char(5) NOT NULL DEFAULT '',
  PRIMARY KEY (`nis`),
  KEY `kd_kelas` (`kd_kelas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `siswa`
--

INSERT INTO `siswa` (`nis`, `namasiswa`, `jeniskelamin`, `alamat`, `tempatlahir`, `tgllahir`, `email`, `kd_kelas`) VALUES
('01-22-2016', 'anggi', 'perempuan', 'palas', 'serang', '1998-02-12', 'anggi@gmail.com', 'XIIPA'),
('02-22-2016', 'Firman', 'laki-laki', 'Taman Krakatau', 'Serang', '1997-05-22', 'maul.2605@gmail.com', 'XIIPA');

-- --------------------------------------------------------

--
-- Table structure for table `soal`
--

CREATE TABLE IF NOT EXISTS `soal` (
  `id_soalujian` char(5) NOT NULL DEFAULT '',
  `kd_mp` char(6) DEFAULT '',
  `kd_kelas` char(5) DEFAULT '',
  `nip` char(11) DEFAULT '',
  `waktu` varchar(10) DEFAULT NULL,
  `tipe` enum('UHAR','UTS','UAS') DEFAULT NULL,
  `tgl_ujian` date DEFAULT NULL,
  PRIMARY KEY (`id_soalujian`),
  KEY `kd_mk` (`kd_mp`),
  KEY `kd_kelas` (`kd_kelas`),
  KEY `nip` (`nip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soal`
--

INSERT INTO `soal` (`id_soalujian`, `kd_mp`, `kd_kelas`, `nip`, `waktu`, `tipe`, `tgl_ujian`) VALUES
('SU001', '111', 'XIIPA', '001', '90', 'UTS', '2016-05-26'),
('SU002', '111', 'XIIPA', '001', '100', 'UHAR', '2016-05-26'),
('SU003', '111', 'XIIPA', '001', '30', 'UTS', '2016-05-27');

-- --------------------------------------------------------

--
-- Table structure for table `soaldetail`
--

CREATE TABLE IF NOT EXISTS `soaldetail` (
  `id_soalujian` char(5) NOT NULL DEFAULT '',
  `id_soal` char(5) NOT NULL DEFAULT '',
  PRIMARY KEY (`id_soalujian`,`id_soal`),
  KEY `id_soal` (`id_soal`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soaldetail`
--

INSERT INTO `soaldetail` (`id_soalujian`, `id_soal`) VALUES
('SU001', 'PG001'),
('SU002', 'PG001'),
('SU003', 'PG001'),
('SU003', 'PG002');

-- --------------------------------------------------------

--
-- Table structure for table `tugas`
--

CREATE TABLE IF NOT EXISTS `tugas` (
  `kd_tugas` int(11) NOT NULL AUTO_INCREMENT,
  `kd_mp` char(6) NOT NULL DEFAULT '0',
  `kd_kelas` char(5) NOT NULL DEFAULT '',
  `judul_tugas` varchar(100) DEFAULT NULL,
  `filename` text,
  PRIMARY KEY (`kd_tugas`),
  KEY `kd_kelas` (`kd_kelas`),
  KEY `kd_mk` (`kd_mp`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tugas`
--

INSERT INTO `tugas` (`kd_tugas`, `kd_mp`, `kd_kelas`, `judul_tugas`, `filename`) VALUES
(1, '111', 'XIIPA', 'Adjnoun', 'Unified Modeling Language.pdf'),
(4, '111', 'XIIPA', 'ga', 'Pengumumanlkmisi.docx'),
(5, '111', 'XIIPA', 'uyu', 'Pengumuman.docx');

-- --------------------------------------------------------

--
-- Structure for view `query_ikutujian`
--
DROP TABLE IF EXISTS `query_ikutujian`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `query_ikutujian` AS select `su`.`id_soalujian` AS `id_soalujian`,`su`.`kd_mp` AS `kd_mp`,`su`.`kd_kelas` AS `kd_kelas`,`su`.`nip` AS `nip`,`su`.`waktu` AS `waktu`,`su`.`tipe` AS `tipe`,`su`.`tgl_ujian` AS `tgl_ujian`,`mp`.`nma_mp` AS `nma_mp`,`g`.`namaguru` AS `namaguru`,`s`.`namasiswa` AS `namasiswa`,`s`.`nis` AS `nis` from (((`soal` `su` join `siswa` `s` on((`su`.`kd_kelas` = `s`.`kd_kelas`))) join `matapelajaran` `mp` on((`su`.`kd_mp` = `mp`.`kd_mp`))) join `guru` `g` on((`su`.`nip` = `g`.`nip`))) group by `su`.`id_soalujian`;

-- --------------------------------------------------------

--
-- Structure for view `query_ikutujiantipe`
--
DROP TABLE IF EXISTS `query_ikutujiantipe`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `query_ikutujiantipe` AS select `query_ikutujian`.`id_soalujian` AS `id_soalujian`,`query_ikutujian`.`kd_mp` AS `kd_mp`,`query_ikutujian`.`kd_kelas` AS `kd_kelas`,`query_ikutujian`.`nip` AS `nip`,`query_ikutujian`.`waktu` AS `waktu`,(case `query_ikutujian`.`tipe` when 'UTS' then 'Ujian Tengah Semester' when 'UAS' then 'Ujian Akhir Semester' when 'UHAR' then 'Ulangan Harian' else 'Ulangan Umum' end) AS `tipe`,`query_ikutujian`.`tgl_ujian` AS `tgl_ujian`,`query_ikutujian`.`nma_mp` AS `nma_mp`,`query_ikutujian`.`namaguru` AS `namaguru`,`query_ikutujian`.`namasiswa` AS `namasiswa`,`query_ikutujian`.`nis` AS `nis` from `query_ikutujian`;

-- --------------------------------------------------------

--
-- Structure for view `query_tampilbanksoal`
--
DROP TABLE IF EXISTS `query_tampilbanksoal`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `query_tampilbanksoal` AS select `banksoalpg`.`id_soal` AS `id_soal`,`matapelajaran`.`nma_mp` AS `nma_mp`,`banksoalpg`.`soal` AS `soal`,`banksoalpg`.`pil_a` AS `pil_a`,`banksoalpg`.`pil_b` AS `pil_b`,`banksoalpg`.`pil_c` AS `pil_c`,`banksoalpg`.`pil_d` AS `pil_d`,`banksoalpg`.`kunci_jwb` AS `kunci_jwb` from (`banksoalpg` join `matapelajaran` on((`banksoalpg`.`kd_mp` = `matapelajaran`.`kd_mp`)));

-- --------------------------------------------------------

--
-- Structure for view `query_tampilbanksoalgurump`
--
DROP TABLE IF EXISTS `query_tampilbanksoalgurump`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `query_tampilbanksoalgurump` AS select `banksoalpg`.`id_soal` AS `id_soal`,`matapelajaran`.`nma_mp` AS `nma_mp`,`banksoalpg`.`soal` AS `soal`,`banksoalpg`.`pil_a` AS `pil_a`,`banksoalpg`.`pil_b` AS `pil_b`,`banksoalpg`.`pil_c` AS `pil_c`,`banksoalpg`.`pil_d` AS `pil_d`,`banksoalpg`.`kunci_jwb` AS `kunci_jwb`,`gurump`.`nip` AS `nip` from ((`banksoalpg` join `matapelajaran` on((`banksoalpg`.`kd_mp` = `matapelajaran`.`kd_mp`))) join `gurump` on((`gurump`.`kd_mp` = `matapelajaran`.`kd_mp`)));

-- --------------------------------------------------------

--
-- Structure for view `query_tampilgurump`
--
DROP TABLE IF EXISTS `query_tampilgurump`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `query_tampilgurump` AS select `gurump`.`nip` AS `nip`,if((`guru`.`jeniskelamin` = 'laki-laki'),concat('Bpk. ',`guru`.`namaguru`),concat('Ibu. ',`guru`.`namaguru`)) AS `namaguru`,`matapelajaran`.`nma_mp` AS `nma_mp`,`gurump`.`kd_kelas` AS `kd_kelas`,`gurump`.`thn_ajaran` AS `thn_ajaran` from (((`guru` join `gurump` on((`gurump`.`nip` = `guru`.`nip`))) join `matapelajaran` on((`gurump`.`kd_mp` = `matapelajaran`.`kd_mp`))) join `kelas` on((`gurump`.`kd_kelas` = `kelas`.`kd_kelas`)));

-- --------------------------------------------------------

--
-- Structure for view `query_tampilgurumpfull`
--
DROP TABLE IF EXISTS `query_tampilgurumpfull`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `query_tampilgurumpfull` AS select `gurump`.`nip` AS `nip`,`guru`.`namaguru` AS `namaguru`,`gurump`.`kd_mp` AS `kd_mp`,`matapelajaran`.`nma_mp` AS `nma_mp`,`gurump`.`kd_kelas` AS `kd_kelas`,`gurump`.`thn_ajaran` AS `thn_ajaran` from (((`gurump` join `guru` on((`gurump`.`nip` = `guru`.`nip`))) join `kelas` on((`gurump`.`kd_kelas` = `kelas`.`kd_kelas`))) join `matapelajaran` on((`gurump`.`kd_mp` = `matapelajaran`.`kd_mp`)));

-- --------------------------------------------------------

--
-- Structure for view `query_tampilkelas`
--
DROP TABLE IF EXISTS `query_tampilkelas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `query_tampilkelas` AS select `kelas`.`kd_kelas` AS `kd_kelas`,`kelas`.`thn_ajaran` AS `thn_ajaran`,if((`guru`.`jeniskelamin` = 'laki-laki'),concat('Bpk. ',`guru`.`namaguru`),concat('Ibu. ',`guru`.`namaguru`)) AS `namaguru` from (`kelas` join `guru` on((`kelas`.`nip` = `guru`.`nip`)));

-- --------------------------------------------------------

--
-- Structure for view `query_tampilmateri`
--
DROP TABLE IF EXISTS `query_tampilmateri`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `query_tampilmateri` AS select `materi`.`kd_materi` AS `kd_materi`,`matapelajaran`.`nma_mp` AS `nma_mp`,`materi`.`kd_kelas` AS `kd_kelas`,`materi`.`judul_materi` AS `judul_materi`,`materi`.`filename` AS `filename` from (`materi` join `matapelajaran` on((`materi`.`kd_mp` = `matapelajaran`.`kd_mp`)));

-- --------------------------------------------------------

--
-- Structure for view `query_tampiltugas`
--
DROP TABLE IF EXISTS `query_tampiltugas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `query_tampiltugas` AS select `tugas`.`kd_tugas` AS `kd_tugas`,`matapelajaran`.`nma_mp` AS `nma_mp`,`tugas`.`kd_kelas` AS `kd_kelas`,`tugas`.`judul_tugas` AS `judul_tugas`,`tugas`.`filename` AS `filename` from (`tugas` join `matapelajaran` on((`tugas`.`kd_mp` = `matapelajaran`.`kd_mp`)));

-- --------------------------------------------------------

--
-- Structure for view `query_ujian`
--
DROP TABLE IF EXISTS `query_ujian`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `query_ujian` AS select `soaldetail`.`id_soalujian` AS `id_soalujian`,`soaldetail`.`id_soal` AS `id_soal`,`banksoalpg`.`soal` AS `soal` from (`banksoalpg` join `soaldetail` on((`soaldetail`.`id_soal` = `banksoalpg`.`id_soal`)));

-- --------------------------------------------------------

--
-- Structure for view `query_walikelas`
--
DROP TABLE IF EXISTS `query_walikelas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `query_walikelas` AS select `kelas`.`nip` AS `nip`,if((`guru`.`jeniskelamin` = 'laki-laki'),concat('Bpk. ',`guru`.`namaguru`),concat('Ibu. ',`guru`.`namaguru`)) AS `namaguru`,`guru`.`alamat` AS `alamat`,`guru`.`email` AS `email`,`kelas`.`kd_kelas` AS `kd_kelas` from (`kelas` join `guru` on((`kelas`.`nip` = `guru`.`nip`)));

--
-- Constraints for dumped tables
--

--
-- Constraints for table `banksoalpg`
--
ALTER TABLE `banksoalpg`
  ADD CONSTRAINT `banksoalpg_ibfk_1` FOREIGN KEY (`kd_mp`) REFERENCES `matapelajaran` (`kd_mp`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `gurump`
--
ALTER TABLE `gurump`
  ADD CONSTRAINT `gurump_ibfk_3` FOREIGN KEY (`kd_kelas`) REFERENCES `kelas` (`kd_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `materi`
--
ALTER TABLE `materi`
  ADD CONSTRAINT `materi_ibfk_1` FOREIGN KEY (`kd_mp`) REFERENCES `matapelajaran` (`kd_mp`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `materi_ibfk_2` FOREIGN KEY (`kd_kelas`) REFERENCES `kelas` (`kd_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `nilai`
--
ALTER TABLE `nilai`
  ADD CONSTRAINT `nilai_ibfk_1` FOREIGN KEY (`id_soalujian`) REFERENCES `soal` (`id_soalujian`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nilai_ibfk_2` FOREIGN KEY (`kd_mk`) REFERENCES `matapelajaran` (`kd_mp`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nilai_ibfk_3` FOREIGN KEY (`kd_kelas`) REFERENCES `kelas` (`kd_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `nilai_ibfk_4` FOREIGN KEY (`nim`) REFERENCES `siswa` (`nis`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `siswa`
--
ALTER TABLE `siswa`
  ADD CONSTRAINT `siswa_ibfk_1` FOREIGN KEY (`kd_kelas`) REFERENCES `kelas` (`kd_kelas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `soal`
--
ALTER TABLE `soal`
  ADD CONSTRAINT `soal_ibfk_1` FOREIGN KEY (`kd_mp`) REFERENCES `matapelajaran` (`kd_mp`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `soal_ibfk_2` FOREIGN KEY (`kd_kelas`) REFERENCES `kelas` (`kd_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `soal_ibfk_3` FOREIGN KEY (`nip`) REFERENCES `guru` (`nip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `soaldetail`
--
ALTER TABLE `soaldetail`
  ADD CONSTRAINT `soaldetail_ibfk_1` FOREIGN KEY (`id_soalujian`) REFERENCES `soal` (`id_soalujian`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tugas`
--
ALTER TABLE `tugas`
  ADD CONSTRAINT `tugas_ibfk_2` FOREIGN KEY (`kd_kelas`) REFERENCES `kelas` (`kd_kelas`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tugas_ibfk_3` FOREIGN KEY (`kd_mp`) REFERENCES `matapelajaran` (`kd_mp`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
