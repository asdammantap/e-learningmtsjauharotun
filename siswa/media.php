<?php
session_start();
error_reporting(0);
if (empty($_SESSION['username']) AND empty($_SESSION['passuser'])){
 include "403.html";
}
else{
include "../koneksi.php";
?>
<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title>E-Learning MA. Jauharotunnaqiyah</title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="vendors/easypiechart/jquery.easy-pie-chart.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        <script src="vendors/modernizr-2.6.2-respond-1.1.0.min.js"></script>
		<link rel="icon" type="image/png" id="favicon"
          href="images/logojauharotunnaqiyah.png"/>
    </head>
    
    <body>
	<?php $username=$_SESSION[username];?>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#"><img src="../img/logojauharotunnaqiyah.png" style="height:17px;width:50px;" />E-Learning</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> <?php echo "$_SESSION[username]";?> <i class="caret"></i>

                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="?p=profil">Profile</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">
                            <li class="active">
                                <a href="#">Beranda</a>
                            </li>
							<li>
                                 <a tabindex="-1" href="?p=datamateri">Materi</a>
                            </li>
                            <li>
                                  <a tabindex="-1" href="?p=datatugas">Tugas</a>
                            </li>
							<li>
                                  <a tabindex="-1" href="?p=datapilihujian">Ujian</a>
                            </li>
                           </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div class="span3" id="sidebar">
                    <ul class="nav nav-list bs-docs-sidenav nav-collapse collapse">
                        <li>
                            <a href="?p=home"><i class="icon-chevron-right"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="?p=gantipass"><i class="icon-chevron-right"></i> Ganti Password</a>
                        </li>
                        <li>
                            <a href="?p=datamateri"><span class="badge badge-success pull-right">
							<?php $tp=mysql_query("SELECT count(kd_materi) as jumlah_data FROM materi, siswa WHERE materi.kd_kelas=siswa.kd_kelas AND siswa.nis='$username'");
							while($r=mysql_fetch_array($tp)){echo "$r[jumlah_data]";}?></span> Materi</a>
                        </li>
                        <li>
                           <a href="?p=datatugas"><span class="badge badge-success pull-right">
							<?php $username=$_SESSION[username];$tp=mysql_query("SELECT count(kd_tugas) as jumlah_data FROM tugas, siswa WHERE tugas.kd_kelas=siswa.kd_kelas AND siswa.nis='$username'");
							while($r=mysql_fetch_array($tp)){echo "$r[jumlah_data]";}?></span> Tugas</a>
                        </li>
						<li>
                            <a href=""><i class="icon-chevron-right"></i> Area Ujian</a>
                        </li>
                        </ul>
                </div>
                
                <!--/span-->
                <div class="span9" id="content">
                    
                    <?php
					include "konten.php";
					?>   
                    
                    
                </div>
            </div>
           </div>
        <!--/.fluid-container-->
		
        <script src="vendors/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/easypiechart/jquery.easy-pie-chart.js"></script>
        <script src="assets/scripts.js"></script>
		<link href="vendors/datepicker.css" link href="vendors/datepicker.css" rel="stylesheet" media="screen">
        <link href="vendors/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet" media="screen" rel="stylesheet" media="screen">
        <link href="vendors/uniform.default.css" rel="stylesheet" media="screen">
        <link href="vendors/chosen.min.css" rel="stylesheet" media="screen">
       <script src="vendors/jquery-1.9.1.js"></script>
        <script src="vendors/jquery.uniform.min.js"></script>
        <script src="vendors/chosen.jquery.min.js"></script>
        <script src="vendors/bootstrap-datepicker.js"></script>

        <script src="vendors/wysiwyg/wysihtml5-0.3.0.js"></script>
        <script src="vendors/wysiwyg/bootstrap-wysihtml5.js"></script>

        <script src="vendors/wizard/jquery.bootstrap.wizard.min.js"></script>

	<script type="text/javascript" src="vendors/jquery-validation/dist/jquery.validate.min.js"></script>
	<script src="assets/form-validation.js"></script>
        
	<script src="assets/scripts.js"></script>
	 <script>

	jQuery(document).ready(function() {   
	   FormValidation.init();
	});
	

        $(function() {
            $(".datepicker").datepicker();
            $(".uniform_on").uniform();
            $(".chzn-select").chosen();
            $('.textarea').wysihtml5();

            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {
                var $total = navigation.find('li').length;
                var $current = index+1;
                var $percent = ($current/$total) * 100;
                $('#rootwizard').find('.bar').css({width:$percent+'%'});
                // If it's the last tab then hide the last button and show the finish instead
                if($current >= $total) {
                    $('#rootwizard').find('.pager .next').hide();
                    $('#rootwizard').find('.pager .finish').show();
                    $('#rootwizard').find('.pager .finish').removeClass('disabled');
                } else {
                    $('#rootwizard').find('.pager .next').show();
                    $('#rootwizard').find('.pager .finish').hide();
                }
            }});
            $('#rootwizard .finish').click(function() {
                alert('Finished!, Starting over!');
                $('#rootwizard').find("a[href*='tab1']").trigger('click');
            });
        });
        </script>
		
</body>
</html>
<?php
}
?>